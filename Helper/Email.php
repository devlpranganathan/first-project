<?php

namespace Hexamarvel\Outofstock\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Hexamarvel\Outofstock\Model\SubscriberFactory;
use Magento\Store\Model\StoreManagerInterface;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;
    protected $subscriberFacotry;
    protected $productRepositoryFactory;
    protected $storeManager;

    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        SubscriberFactory $subscriberFactory,
        ProductRepositoryInterfaceFactory $productRepositoryFactory,
        StoreManagerInterface $storeManager,
        $data = []
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();
        $this->subscriberFactory = $subscriberFactory;
        $this->productRepositoryFactory = $productRepositoryFactory;
        $this->storeManager = $storeManager;
    }

    public function sendProductId($productId)
    {
        $subscribers = $this->subscriberFactory->create()->getCollection();
        foreach ($subscribers as $subscriber) {
            if ($subscriber->getProductId() == $productId)
            {
                $name = $subscriber->getCustomerName();
                $customerId = $subscriber->getCustomerId();
                $email = $subscriber->getEmail();
                $productData = $this->productRepositoryFactory->create()->getById($productId);
                $productName = $productData->getName();
                $productUrlKey = $productData->getUrlKey();
                $storeUrl = $this->storeManager->getStore()->getBaseUrl();
                $store = $this->storeManager->getStore();
                $productUrl = $storeUrl .$productUrlKey.'.html';
                $productImagePath = $productData->getData('image');
                $image_url = ("//catalog/product" . $productImagePath);

                $this->sendEmail($name, $productId,$customerId, $productName, $email,$productUrl,$image_url);
            }
        }
    }

    public function sendEmail($name,$productId,$customerId, $productName, $email, $productUrl,$image_url)
    {
        try {
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => 'Owner',
                'email' => $email

            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('outofstock_item')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'name'  => $name,
                    'productname' => $productName,
                    'producturl'=> $productUrl,
                    'productimg' => $image_url
                ])
                ->setFrom($sender)
                ->addTo($sender['email'], $name)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();

            //deleting a customer from db after sending email
            $subscriber = $this->subscriberFactory->create();
            $collection = $subscriber->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $customerId])
                ->addFieldToFilter('product_id', ['eq' => $productId]);
            if($collection->Count() >0)
            {
               $collection->walk('delete');
            }

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
