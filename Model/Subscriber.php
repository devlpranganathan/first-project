<?php


namespace Hexamarvel\Outofstock\Model;

class Subscriber extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Hexamarvel\Outofstock\Model\ResourceModel\Subscriber');
    }
}
