<?php

namespace Hexamarvel\Outofstock\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Subscriber extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('subscribers', 'id');
    }
}
