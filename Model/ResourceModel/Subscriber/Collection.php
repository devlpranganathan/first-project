<?php

namespace Hexamarvel\Outofstock\Model\ResourceModel\Subscriber;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Hexamarvel\Outofstock\Model\Subscriber;
use Hexamarvel\Outofstock\Model\ResourceModel\Subscriber as SubscriberResourceModel;


class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(Subscriber::class, SubscriberResourceModel::class);
    }
}
