<?php

namespace Hexamarvel\Outofstock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Hexamarvel\Outofstock\Helper\Email;
use Hexamarvel\Outofstock\Model\SubscriberFactory;

class ProductInStockObserver implements ObserverInterface
{
    protected $helperEmail;
    protected $subscriber;  

    public function __construct(Email $helperEmail,SubscriberFactory $subscriber)
    {
        $this->helperEmail = $helperEmail;
        $this->subscriber = $subscriber;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $product = $observer->getProduct();
        $stockData = $product->getStockData();
        $stockStatus = $stockData['is_in_stock'];
       
        //Check if the stock status is available
        if ($stockStatus == "1") {
            $stock = $this->subscriber->create()->load($product->getId());
            $this->helperEmail->sendProductId($product->getId());
        } else {
            $stock = $this->subscriber->create()->load($product->getId());
            return;
        }
    }
}
