<?php

namespace Hexamarvel\Outofstock\Controller\Add;

use Magento\Framework\App\Action\Context;
use Hexamarvel\Outofstock\Model\SubscriberFactory;

class Create extends \Magento\Framework\App\Action\Action
{

    protected $scopeConfig;

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        SubscriberFactory $subsciber
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->subscriber = $subsciber;
        parent::__construct($context);
    }

    public function execute()
    {
        try {

            $resultRedirect = $this->resultRedirectFactory->create();
            $params         = $this->getRequest()->getParams();
            $email = $params['email'];
            $prodcutId = $params['product_id'];
            $subscriber = $this->subscriber->create();
            $collection = $subscriber->getCollection()
                ->addFieldToFilter('email', ['eq' => $email])
                ->addFieldToFilter('product_id', ['eq' => $prodcutId]);

            // check if collection count > 0
            if ($collection->Count() == 0) {
                $newSubscriber  = $this->subscriber->create();
                //getting data from collection 
                $newSubscriber->setData($params);
                $newSubscriber->save();
                $this->messageManager->addSuccess(__('Your Email has been added to the list'));
            } else {
                $this->messageManager->addError(_('Email Already Exists'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(_('error'));
        }
    return $resultRedirect->setPath($this->_redirect->getRefererUrl());
    }
}
