<?php

namespace Hexamarvel\Outofstock\Block;

use Hexamarvel\Outofstock\Model\Subscriber;
use Magento\Store\Model\StoreManagerInterface;

class StockCheck extends \Magento\Framework\View\Element\Template
{

    protected $customerSession;
    protected $storeManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,

        array $data = []
    ) {
        $this->_registry = $registry;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;


        parent::__construct($context, $data);
    }
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function checkLogin()
    {
        if($this->customerSession->isLoggedIn())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getCustomerName()
    {
        return $this->customerSession->getCustomer()->getName();
    }
    public function getCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }

    public function getLoginPageUrl()
    {
        $storeUrl = $this->storeManager->getStore()->getBaseUrl();
        return $storeUrl;
    }

}